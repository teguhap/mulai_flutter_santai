import 'dart:io';
import 'package:flutter/material.dart';
import 'package:mulai_flutter_santai/view/home_page.dart';
import 'package:mulai_flutter_santai/view/login_page.dart';
import 'package:mulai_flutter_santai/theme/theme.dart';
import 'package:mulai_flutter_santai/view/main_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: lightTheme,
      routes: {
        '/loginPage': (context) => LoginPage(),
        '/homePage': (context) => HomePage(
              namaUser: '',
            ),
      },
      home: MainPage(),
      // initialRoute: '/loginPage',
    );
  }
}
