import 'package:flutter/material.dart';
import 'package:mulai_flutter_santai/config/config.dart';
import 'package:mulai_flutter_santai/model/film_model.dart';

class RowFilm extends StatelessWidget {
  FilmModel filmModel;
  RowFilm({
    super.key,
    required this.filmModel,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 120,
      margin: EdgeInsets.only(right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 150,
            width: 120,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                      '$baseImageUrl/w500${filmModel.posterPath ?? ''}'),
                  fit: BoxFit.fill,
                ),
                borderRadius: BorderRadius.circular(10)),
          ),
          Text(
            filmModel.originalTitle ?? '',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Text(
            filmModel.originalTitle ?? '',
            style: TextStyle(fontSize: 10),
          ),
        ],
      ),
    );
  }
}
