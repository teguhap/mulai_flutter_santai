import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mulai_flutter_santai/theme.dart';
import 'package:mulai_flutter_santai/view/explore_page.dart';
import 'package:mulai_flutter_santai/view/home_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List listOfPage = [
    HomePage(namaUser: ''),
    ExplorePage(),
    ExplorePage(),
    ExplorePage(),
  ];
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listOfPage[currentIndex],
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            offset: Offset(
              0,
              6,
            ),
            blurRadius: 20,
          ),
        ]),
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
              onPressed: () {
                setState(() {
                  currentIndex = 0;
                });
              },
              icon: Icon(Icons.home),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  currentIndex = 1;
                });
              },
              icon: Icon(
                Icons.home,
                color: currentIndex == 1 ? mainColor : Colors.black,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.home),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.home),
            ),
          ],
        ),
      ),
    );
  }
}
