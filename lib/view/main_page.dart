import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mulai_flutter_santai/theme.dart';
import 'package:mulai_flutter_santai/view/explore_page.dart';
import 'package:mulai_flutter_santai/view/home_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List listOfPage = [
    HomePage(namaUser: ''),
    ExplorePage(),
    ExplorePage(),
    ExplorePage(),
  ];
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listOfPage[currentIndex],
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 10,
            )
          ],
        ),
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: BottomNavigationBar(
          selectedItemColor: mainColor,
          unselectedItemColor: Colors.grey,
          currentIndex: currentIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          elevation: 0,
          type: BottomNavigationBarType.fixed,
          onTap: (value) {
            print(value);
            setState(() {
              currentIndex = value;
            });
          },
          items: [
            BottomNavigationBarItem(
              label: 'Home',
              icon: Column(
                children: [
                  ImageIcon(
                    AssetImage(
                      'assets/icons/ic_home.png',
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  currentIndex == 0
                      ? Container(
                          width: 30,
                          height: 3,
                          decoration: BoxDecoration(
                              color: mainColor,
                              borderRadius: BorderRadius.circular(10)),
                        )
                      : Container(),
                ],
              ),
            ),
            BottomNavigationBarItem(
              label: 'explore',
              icon: Column(
                children: [
                  ImageIcon(
                    AssetImage(
                      'assets/icons/ic_search3.png',
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  currentIndex == 1
                      ? Container(
                          width: 30,
                          height: 3,
                          decoration: BoxDecoration(
                              color: mainColor,
                              borderRadius: BorderRadius.circular(10)),
                        )
                      : Container(),
                ],
              ),
            ),
            BottomNavigationBarItem(
              label: 'explore',
              icon: Column(
                children: [
                  ImageIcon(
                    AssetImage(
                      'assets/icons/ic_home.png',
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  currentIndex == 2
                      ? Container(
                          width: 30,
                          height: 3,
                          decoration: BoxDecoration(
                              color: mainColor,
                              borderRadius: BorderRadius.circular(10)),
                        )
                      : Container(),
                ],
              ),
            ),
            BottomNavigationBarItem(
              label: 'explore',
              icon: Column(
                children: [
                  ImageIcon(
                    AssetImage(
                      'assets/icons/ic_home.png',
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  currentIndex == 3
                      ? Container(
                          width: 30,
                          height: 3,
                          decoration: BoxDecoration(
                              color: mainColor,
                              borderRadius: BorderRadius.circular(10)),
                        )
                      : Container(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
