import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mulai_flutter_santai/config/config.dart';
import 'package:mulai_flutter_santai/model/film_model.dart';
import 'package:mulai_flutter_santai/model/popular_model_response.dart';

class GetFilm {
  Future<List<FilmModel>> getPopularFilm() async {
    var url = Uri.parse('${baseUrl}movie/popular?api_key=$apiKey');

    // Await the http get response, then decode the json-formatted response.
    print(url);
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      print(jsonResponse);
      PopularModelResponse popularModelResponse =
          PopularModelResponse.fromJson(jsonResponse);
      return popularModelResponse.results ?? [];
    } else {
      print('Request failed with status: ${response.statusCode}.');
      return [];
    }
  }

  Future<List<FilmModel>> getTopRatedFilm() async {
    var url = Uri.parse('${baseUrl}movie/top_rated?api_key=$apiKey');

    // Await the http get response, then decode the json-formatted response.
    print(url);
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      print(jsonResponse);
      PopularModelResponse popularModelResponse =
          PopularModelResponse.fromJson(jsonResponse);
      return popularModelResponse.results ?? [];
    } else {
      print('Request failed with status: ${response.statusCode}.');
      return [];
    }
  }
}
