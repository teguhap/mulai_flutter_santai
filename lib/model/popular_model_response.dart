// To parse this JSON data, do
//
//     final popularModelResponse = popularModelResponseFromJson(jsonString);

import 'dart:convert';

import 'package:mulai_flutter_santai/model/film_model.dart';

PopularModelResponse popularModelResponseFromJson(String str) =>
    PopularModelResponse.fromJson(json.decode(str));

String popularModelResponseToJson(PopularModelResponse data) =>
    json.encode(data.toJson());

class PopularModelResponse {
  int? page;
  List<FilmModel>? results;
  int? totalPages;
  int? totalResults;

  PopularModelResponse({
    this.page,
    this.results,
    this.totalPages,
    this.totalResults,
  });

  factory PopularModelResponse.fromJson(Map<String, dynamic> json) =>
      PopularModelResponse(
        page: json["page"],
        results: json["results"] == null
            ? []
            : List<FilmModel>.from(
                json["results"]!.map((x) => FilmModel.fromJson(x))),
        totalPages: json["total_pages"],
        totalResults: json["total_results"],
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "results": results == null
            ? []
            : List<dynamic>.from(results!.map((x) => x.toJson())),
        "total_pages": totalPages,
        "total_results": totalResults,
      };
}
